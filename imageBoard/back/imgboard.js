const express = require('express');
const router = express.Router();
const file = require('./files');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('./config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const  upload = multer({storage});

router.get('/', (req, res) => {
  let imageBoard = file.getItems();
  if (req.query.orderBy === 'date' && req.query.direction === 'desc'){
    imageBoard.reverse();
  }
  res.send(imageBoard);
});

router.get('/:id', (req, res) => {
  const imageBoard = file.getItem(req.params.id);
  if (!imageBoard) {
    res.status(404).send({message: 'not found'})
  }
  return res.send(imageBoard);
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.text) {
      return res.status(404).send({message: 'text are required!'});
    }
    const imageBoard = {
      author: '',
      text: req.body.text,
    };

    if (req.body.author){
      imageBoard.author = req.body.author;
    }
    if (req.file){
      imageBoard.image = req.file.filename;
    }

    await file.addItem(imageBoard);

    return res.send({message: 'created new message', id: imageBoard.id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;