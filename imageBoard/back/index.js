const express = require('express');
const app = express();
const file = require('./files');
const imgBoard = require('./imgboard');
const cors = require('cors');
const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.static('public'));
app.use(express.json());
app.use('/imgboard', imgBoard);

const run =async () => {
  await file.init();
}

app.listen(port, () => {
  console.log('We are live on ' + port);
});

run().catch(e => console.error(e));