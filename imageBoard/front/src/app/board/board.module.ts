export class IbModel {
  constructor(
    public author: string,
    public text: string,
    public image: string,
    public id: string,
    public date: string,
  ) {}
}

export interface IbData {
  [key: string]: any;
  author: string;
  text: string;
  image: File | null;
}
