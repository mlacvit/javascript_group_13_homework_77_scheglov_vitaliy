import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IbData, IbModel } from '../board/board.module';
import { IbService } from '../ib.service';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, OnDestroy {
@ViewChild('f') forma!: NgForm;
  fetchingSubscriptions!: Subscription;
  isFetching: boolean = false;
  imageBoard: IbModel[] | null = null;
  changeSub!: Subscription;
  fetchSub!: Subscription;
  isFetchingIB: boolean = false;
  apiUrl = environment.apiUrl;
  constructor(private service: IbService) { }

  ngOnInit(): void {
    this.service.getIB();
    this.changeSub = this.service.ibArrayChange.subscribe( (ib: IbModel[]) => {
      this.imageBoard = ib;
    });

    this.fetchingSubscriptions = this.service.fetchIB.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });

    this.fetchSub = this.service.fetching.subscribe( (isFech: boolean) => {
      this.isFetching = isFech;
    });
  }

  onSub() {
    const value: IbData = this.forma.value;
    this.service.createIB(value).subscribe(()=>{
      this.service.getIB();
    });
  };


  ngOnDestroy(): void {
    this.fetchingSubscriptions.unsubscribe();
    this.changeSub.unsubscribe();
    this.fetchSub.unsubscribe();
  };


}
