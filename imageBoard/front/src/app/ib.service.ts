import { Injectable } from '@angular/core';
import { IbData, IbModel } from './board/board.module';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject, tap } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IbService {
  ibArray: IbModel[] | null = null;
  ibArrayChange = new Subject<IbModel[]>();
  fetchIB = new Subject<boolean>();
  fetching = new Subject<boolean>();
  constructor(private http: HttpClient) { }

  createIB(IbData: IbData) {
    const formData = new FormData();
    Object.keys(IbData).forEach(key => {
      if (IbData[key] !== null){
        formData.append(key, IbData[key]);
      }
    });
    this.fetchIB.next(true);
    return this.http.post(environment.apiUrl + '/imgboard', formData).pipe(
      tap(() => {
        this.fetchIB.next(false);
      }, error => {
        this.fetchIB.next(false);
      })
    );
  }

  getIB() {
    this.fetching.next(true);
    return this.http.get<IbModel[]>(environment.apiUrl + '/imgboard?orderBy=date&direction=desc').pipe(
      map(response => {
        return response.map(ib => {
          return new IbModel(
            ib.author,
            ib.text,
            ib.image,
            ib.id,
            ib.date,
                            )
        });
      })
    ).subscribe({
      next: result => {
        this.ibArray = result;
        this.ibArrayChange.next(this.ibArray.slice());
        this.fetching.next(false);
      },
      error: err => {
        this.fetching.next(false);
        console.log(err);
      }
    });
  }




}
